
<section class="ds color darker page_copyright section_padding_15">
				<div class="container">
					<div class="row">
						<div class="col-sm-12 text-center">
							<p class="fontsize_12">&copy; <a target="_blank" href="https://inovtechsenegal.com/">Inovtech</a> Copyright 2019. All Rights Reserved.</p>
							<p class="darklinks"> 
								<a class="social-icon soc-facebook" href="#" title="Facebook"></a> 
								<a class="social-icon soc-twitter" href="#" title="Twitter"></a> 
								<a class="social-icon soc-google" href="#" title="Twitter"></a> 
								<a class="social-icon soc-linkedin" href="#" title="Twitter"></a>
								<a class="social-icon soc-youtube" href="#" title="Youtube"></a> 
							</p>
						</div>
					</div>
				</div>
			</section>