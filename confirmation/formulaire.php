<section class="page_breadcrumbs ds color parallax section_padding_top_75 section_padding_bottom_75">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 text-center">
                <h2>Confirmation de votre commmande</h2>

            </div>
        </div>
    </div>
</section>
<section class="ls section_padding_top_100 section_padding_bottom_100">
    <div class="container">
        <div class="row">
            <form class="shop-register" role="form" method="POST" action="Controller/mail/mail.php">
                <div class="col-sm-6">
                    <div class="form-group validate-required" id="billing_first_name_field"> <label
                            for="billing_first_name" class="control-label">
                            <span class="grey">Prénom:</span>
                            <span class="required">*</span>
                        </label> <input type="text" class="form-control " required name="prenom" id="billing_first_name"
                            placeholder="" value=""> </div>
                    <div class="form-group" id="billing_company_field"> <label for="billing_company"
                            class="control-label">
                            <span class="grey">Nom De La Compagnie:</span>
                        </label> <input type="text" class="form-control " required name="company" id="billing_company"
                            placeholder="" value=""> </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group validate-required" id="billing_last_name_field"> <label
                            for="billing_last_name" class="control-label">
                            <span class="grey">Nom:</span>
                            <span class="required">*</span>
                        </label> <input type="text" class="form-control " required name="nom" id="billing_last_name"
                            placeholder="" value=""> </div>
                    <div class="form-group validate-required validate-email" id="billing_email_field"> <label
                            for="billing_email" class="control-label">
                            <span class="grey">Adresse Mail:</span>
                            <span class="required">*</span>
                        </label> <input type="email" class="form-control " required name="email" id="billing_email"
                            placeholder="" value=""> </div>
                </div>
                <div class="col-sm-12">
                    <div class="form-group address-field validate-required" id="billing_address_fields"> <label
                            for="billing_address_1" class="control-label">
                            <span class="grey">Addresse:</span>
                            <span class="required">*</span>
                        </label> <input type="text" class="form-control " required name="adresse" id="billing_address_1"
                            placeholder="" value=""> </div>
                </div>
                <div class="col-sm-12">
                    <br> <br> <br>
                <table>
                    <?php
                    require_once 'Model/DB.php';
                    require_once 'Model/ProduitController.php';

                    if(!isset($_SESSION['panier']))
                    {
                        $_SESSION['panier'] = array();
                    }

                    $produit = array();
                    foreach($_SESSION['panier'] as $produitid)
                    {
                        $produit[] = mysqli_fetch_row(listeProduitById($produitid));
                    }

                   $total =0;
                    foreach($produit as $key => $item)
                    {
                    ?>
                    

                    <tr class="cart_item">

                        <td class="product-info">
                            <div class="media">
                                <div class="media-left"> <a href="#">
                                        <img class="media-object cart-product-image"
                                            src="images/produits/<?php echo $item[6]; ?>" alt="">
                                    </a> </div>
                                <div class="media-body">
                                    <h4 class="media-heading">
                                        <a href="#"><?php echo $item[1]; ?> </a>
                                    </h4>
                                </div>
                            </div>
                        </td>

                        <td class="product-price"> <span class="amount"><?php echo $item[4]; $total+=$item[4] ?></span>
                        <span class="currencies">F CFA </span> </td>
                        <td class="product-quantity">
                                <div class="quantity"> 
                                    <input type="button" value="-" class="minus">
                                    <input type="number" step="1" min="0" name="quantite_produit['<?php echo $item[1]; ?>']" value="1" title="Qty" class="form-control quantite">
                                    <input type="button" value="+" class="plus"> 
                                </div>
                        </td>
                        <td class="product-subtotal"> <span><?php echo $item[3]; ?></span></td>
                        <td class="total_produit"><?php echo $item[4]; ?></td>
                    </tr>
                    <!-- <input type="hidden" name="nom_produit[]" value="<?php //echo $item[1]; ?>"> -->
                    <?php
                    }
                    ?>
                    <tr>
                    <td colspan='100%'><hr></td>
                    </tr>
                    <tr>
                        <td colspan="40%"  style="text-align:right;">Total</td>
                        <td> <b><span id="total_general"> <?php  echo $total; ?></span> FCFA</b> </td>
                    </tr>
                    </table>
                </div>
                <input type="hidden" name="secure" value="ok">
                <div class="col-sm-12">
                    <button type="submit" class="theme_button wide_button color1 topmargin_40">Envoyer</button>
                    <button type="reset" class="theme_button wide_button">Clear Form</button>
                </div>
            </form>
        </div>
    </div>
</section>

<script> 
$(document).ready(function(){
    $('.plus').click(function(){
       
        var $row = $(this).closest("tr");
        var $text = $row.find(".amount").text();
        // var quantite = parseInt($row.find(".quantite").text());//quantite
        var quantite = parseInt($row.find(".quantite").val())+1;
        
        if(quantite>=1)
       {
            var nombre = parseInt($text); 
            $row.find(".total_produit").text(quantite*nombre);
        // alert(quantite*nombre);
       }


       var total = 0;
       $(".total_produit").each(function() {
        total += parseInt($(this).text());
            // alert($(this).text());
        });
        
        $('#total_general').text(total);


    });
    $('.minus').click(function(){
       
       var $row = $(this).closest("tr");
       var $text = $row.find(".amount").text();
       // var quantite = parseInt($row.find(".quantite").text());//quantite
       var quantite = parseInt($row.find(".quantite").val())-1;
       if(quantite>=1)
       {
            var nombre = parseInt($text); 
            $row.find(".total_produit").text(quantite*nombre);
        // alert(quantite*nombre);
       }

       var total = 0;
       $(".total_produit").each(function() {
        total += parseInt($(this).text());
            // alert($(this).text());
        });
        
        $('#total_general').text(total);

       
       
   });
});
</script>