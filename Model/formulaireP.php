<section class="page_breadcrumbs ds color parallax section_padding_top_75 section_padding_bottom_75">
<!-- $sql = "INSERT INTO produit Values(null,$nom,$categorie,$desciption,$prix,$quantite,$image) "; -->
    <div class="container">
        <div class="row">
            <div class="col-sm-12 text-center">
                <h2>Produit</h2>

            </div>
        </div>
    </div>
</section>
<section class="ls section_padding_top_100 section_padding_bottom_100">
    <div class="container">
        <div class="row">
            <form class="shop-register" role="form" method="POST" action="../controller/ProdController.php">
                <div class="col-sm-6">
                    <div class="form-group validate-required" id="billing_first_name_field"> <label
                            for="billing_first_name" class="control-label">
                            <span class="grey">Nom:</span>
                            <span class="required">*</span>
                        </label> <input type="text" class="form-control " required name="nom"
                            id="billing_first_name" placeholder="" value=""> </div>
                    <div class="form-group" id="billing_company_field"> <label for="billing_company"
                            class="control-label">
                            <span class="grey">Categorie:</span>
                            <span class="required">*</span>
                            <select id="position" class="form-control" name="categorie">
                                <option></option>
                                <?php
                                    require_once 'DB.php';
                                    require_once 'ProduitController.php';
                                    $liste_cat = listeCategorie();
                                    while($row = mysqli_fetch_row($liste_cat))
                                    {
                                        echo "<option value='$row[0]'>$row[1]</option>";
                                    }
                                ?>                                                                                                                         
                            </select>
                </div>
                <div class="col-sm-6">
                    <div class="form-group validate-required" id="billing_last_name_field"> <label
                            for="billing_last_name" class="control-label">
                            <span class="grey">Description:</span>
                            <span class="required">*</span>
                        </label> <input type="text" class="form-control " required name="description"
                            id="billing_last_name" placeholder="" value=""> </div>
                    <div class="form-group validate-required validate-email" id="billing_email_field"> <label
                            for="billing_email" class="control-label">
                            <span class="grey">Quantité:</span>
                            <span class="required">*</span>
                        </label> <input type="text" class="form-control " required name="quantite"
                            id="billing_email" placeholder="" value=""> </div>
                </div>
                <div class="col-sm-12">
                    <div class="form-group address-field validate-required" id="billing_address_fields"> <label
                            for="billing_address_1" class="control-label">
                            <span class="grey">Image:</span>
                            <span class="required">*</span>
                        </label> <input type="file" class="form-control " required name="image"
                            > </div>
                </div>
                
                    <input type="hidden" name="secure" value="ok">
                <div class="col-sm-12">
                    <button type="submit" class="theme_button wide_button color1 topmargin_40" name="valider">Envoyer</button>
                    <button type="reset" class="theme_button wide_button">Clear Form</button>
                </div>
            </form>
        </div>
    </div>
</section>
