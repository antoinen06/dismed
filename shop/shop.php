<section class="ls section_padding_top_100 section_padding_bottom_75 columns_padding_25">
				<div class="container">
					<div class="row">
						<div class="col-sm-7 col-md-8 col-lg-9 col-sm-push-5 col-md-push-4 col-lg-push-3">
							<div class="table-responsive">
								<table class="table shop_table cart cart-table">
									<thead>
										<tr>
											<td class="product-info">Product</td>
											<td class="product-price-td">Price</td>
											<td class="product-quantity">Quantity</td>
											<td class="product-subtotal">Subtotal</td>
											<td class="product-remove">&nbsp;</td>
										</tr>
									</thead>
									<tbody>
										<tr class="cart_item">
											<td class="product-info">
												<div class="media">
													<div class="media-left"> <a href="shop-product-right.html">
												<img class="media-object cart-product-image" src="images/shop/01.png" alt="">
											</a> </div>
													<div class="media-body">
														<h4 class="media-heading"> <a href="shop-product-right.html">At vero eos et accusam</a> </h4> <span class="grey">Color:</span> Black<br> <span class="grey">Size:</span> M </div>
												</div>
											</td>
											<td class="product-price"> <span class="currencies">$</span><span class="amount">199.00</span> </td>
											<td class="product-quantity">
												<div class="quantity"> <input type="button" value="-" class="minus"> <input type="number" step="1" min="0" name="product_quantity" value="1" title="Qty" class="form-control"> <input type="button" value="+" class="plus"> </div>
											</td>
											<td class="product-subtotal"> <span class="currencies">$</span><span class="amount">199.00</span> </td>
											<td class="product-remove"> <a href="#" class="remove fontsize_20" title="Remove this item">
										<i class="fa fa-trash-o"></i>
									</a> </td>
										</tr>
										<tr class="cart_item">
											<td class="product-info">
												<div class="media">
													<div class="media-left"> <a href="shop-product-right.html">
												<img class="media-object cart-product-image" src="images/shop/02.png" alt="">
											</a> </div>
													<div class="media-body">
														<h4 class="media-heading"> <a href="shop-product-right.html">Justo duo dolores et ea rebum</a> </h4> <span class="grey">Size:</span> XL </div>
												</div>
											</td>
											<td class="product-price"> <span class="currencies">$</span><span class="amount">100.00</span> </td>
											<td class="product-quantity">
												<div class="quantity"> <input type="button" value="-" class="minus"> <input type="number" step="1" min="0" name="product_quantity" value="1" title="Qty" class="form-control"> <input type="button" value="+" class="plus"> </div>
											</td>
											<td class="product-subtotal"> <span class="currencies">$</span><span class="amount">100.00</span> </td>
											<td class="product-remove"> <a href="#" class="remove fontsize_20" title="Remove this item">
										<i class="fa fa-trash-o"></i>
									</a> </td>
										</tr>
									</tbody>
								</table>
							</div>
							<div class="cart-buttons"> <a class="theme_button" href="#">Countinue Shopping</a> <input type="submit" class="theme_button color1" name="update_cart" value="Update Cart"> <button type="submit" class="theme_button color2">Proceed to Checkout</button> </div>
							<div class="cart-collaterals">
								<div class="cart_totals">
									<h4>Cart Totals</h4>
									<table class="table">
										<tbody>
											<tr class="cart-subtotal">
												<td>Cart Subtotal</td>
												<td><span class="currencies">$</span><span class="amount">299.00</span> </td>
											</tr>
											<tr class="shipping">
												<td>Shipping and Handling</td>
												<td> Free Shipping </td>
											</tr>
											<tr class="order-total">
												<td class="grey">Order Total</td>
												<td><strong class="grey"><span class="currencies">$</span><span class="amount">299.00</span> </strong> </td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>

						</div>
						<!--eof .col-sm-8 (main content)-->
						<!-- sidebar -->

						<!-- eof aside sidebar -->
					</div>
				</div>
			</section>