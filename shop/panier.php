<?php
//db et produit controller sont deja appeler avec /panier.php
 if(!isset($_SESSION['panier']))
 {
	$_SESSION['panier'] = array();
 }
 $produit = array();
 foreach($_SESSION['panier'] as $produitid)
 {
	$produit[] = mysqli_fetch_row(listeProduitById($produitid));
 }

//   print_r($produit);
?>
<section class="page_breadcrumbs ds color parallax section_padding_top_75 section_padding_bottom_75">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 text-center">
                <h2>Panier</h2>

            </div>
        </div>
    </div>
</section>
<section class="ls section_padding_top_100 section_padding_bottom_75 columns_padding_25">
    <div class="container">

        <div class="row">
            <div class="col-sm-7 col-md-8 col-lg-9 col-sm-push-5 col-md-push-4 col-lg-push-2">
                <div class="table-responsive">
                    <table class="table shop_table cart cart-table">
                        <thead>
                            <tr>
                                <td class="product-info">Product</td>
                                <td class="product-price-td">Prix</td>
                                <!-- <td class="product-quantity">Quantity</td> -->
                                <!-- <td class="product-subtotal">Subtotal</td> -->
                                <td class="product-remove">Action</td>
                            </tr>
                        </thead>
                        <tbody>

                            <!-- foreach -->
                            <?php
								foreach($produit as $key => $item)
								{	
						?>
                            <tr class="cart_item">
                                <td class="product-info">
                                    <div class="media">
                                        <div class="media-left"> <a href="shop-product-right.html">
                                                <img class="media-object cart-product-image"
                                                    src="images/produits/<?php echo $item[6]; ?>" alt="">
                                            </a> </div>
                                        <div class="media-body">
                                            <h4 class="media-heading"> <a href="#"> <?php echo $item[1]; ?>
                                                </a> </h4> <span class="grey">Description:</span>
                                            <?php echo $item[3]; ?><br>
                                        </div>
                                    </div>
                                </td>
                                <!-- <td class="product-price"> <span class="amount"><?php echo $item[4]; ?></span><span -->
                                        <!-- class="currencies">F CFA </span> </td> -->

                                <!-- <td class="product-quantity">
                                    <div class="quantity"> <input type="button" value="-" class="minus"> <input
                                            type="number" step="1" min="0" name="product_quantity" value="1" title="Qty"
                                            class="form-control"> <input type="button" value="+" class="plus"> </div>
                                </td> -->
                              
                                <td class="product-subtotal"> <span class="amount"><?php echo $item[4]; ?></span><span
                                        class="currencies">F CFA </span> </td>
                                <td class="product-remove"> <a href="#" id="delete"
                                        onclick="deletes(<?php echo $item[0]; ?>)" class="remove fontsize_20"
                                        title="Remove this item">
                                        <i class="fa fa-trash-o"></i>
                                    </a> <input type="hidden" id="valeurekey" value="<?php echo $item[0];  ?>">
                                </td>
                            </tr>

                            <!-- endforeach -->
                            <?php
							}
						
							?>
                           
                            <!-- <tr class="cart_item">
                                <td class="product-info">
                                    <div class="media">
                                        <div class="media-left"> <a href="shop-product-right.html">
                                                <img class="media-object cart-product-image" src="images/shop/02.png"
                                                    alt="">
                                            </a> </div>
                                        <div class="media-body">
                                            <h4 class="media-heading"> <a href="shop-product-right.html">Justo duo
                                                    dolores et ea rebum</a> </h4> <span class="grey">Size:</span> XL
                                        </div>
                                    </div>
                                </td>
                                <td class="product-price"> <span class="currencies">$</span><span
                                        class="amount">100.00</span> </td>
                                <td class="product-quantity">
                                    <div class="quantity"> <input type="button" value="-" class="minus"> <input
                                            type="number" step="1" min="0" name="product_quantity" value="1" title="Qty"
                                            class="form-control"> <input type="button" value="+" class="plus"> </div>
                                </td>
                                <td class="product-subtotal"> <span class="currencies">$</span><span
                                        class="amount">100.00</span> </td>
                                <td class="product-remove"> <a href="#" class="remove fontsize_20"
                                        title="Remove this item">
                                        <i class="fa fa-trash-o"></i>
                                    </a> </td>
							</tr> -->

                        </tbody>
                    </table>
                </div>
                <script>
                            function deletes(key) {
                                console.log(key)
                                $.ajax({
                                    url: 'Controller/ProduitController.php?key=' + key, // La ressource ciblée
                                    type: 'GET',
                                    success: function(data) {
                                            // $('#count_cart').attr('data-count',<?php echo count($_SESSION['panier']); ?>);
                                           //    console.log( $('#count_cart').attr('data-count'));
                                           $('#count_cart').attr('data-count',data);
                                        }
                                    
                                });
                            }
                            
                </script>
                <div class="cart-buttons"> <a class="theme_button" href="../produit.php">Countinue Shopping</a>
                    <a class="theme_button color2" href="../confirmation.php"> Proceed to Checkout</a></div>
                <div class="cart-collaterals">
                    <!-- <div class="cart_totals">
                        <h4>Cart Totals</h4>
                        <table class="table">
                            <tbody>
                                <tr class="cart-subtotal">
                                    <td>Cart Subtotal</td>
                                    <td><span class="currencies">$</span><span class="amount">299.00</span> </td>
                                </tr>
                                <tr class="shipping">
                                    <td>Shipping and Handling</td>
                                    <td> Free Shipping </td>
                                </tr>
                                <tr class="order-total">
                                    <td class="grey">Order Total</td>
                                    <td><strong class="grey"><span class="currencies">$</span><span
                                                class="amount">299.00</span> </strong> </td>
                                </tr>
                            </tbody>
                        </table>
                    </div> -->
                </div>

            </div>
            <!--eof .col-sm-8 (main content)-->
            <!-- sidebar -->

            <!-- eof aside sidebar -->
        </div>
    </div>
</section>