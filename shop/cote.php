<aside class="col-sm-5 col-md-4 col-lg-3 col-sm-pull-7 col-md-pull-8 col-lg-pull-9">
    <div class="widget widget_search">
        <h3 class="widget-title">Search</h3>
        <form method="get" class="searchform" action="http://webdesign-finder.com/html/pharma/">
            <div class="form-group"> <label class="sr-only" for="widget-search">Search for:</label> <input
                    id="widget-search" type="text" value="" name="search" class="form-control"
                    placeholder="Search Here..."> </div> <button type="submit"
                class="theme_button color1 no_bg_button">Search</button>
        </form>
    </div>
    <div class="widget widget_categories">
        <h3 class="widget-title">Categories</h3>
        <ul class="greylinks list2 checklist">
            <li class="active"> <a href="blog-right.html" title="" class="content-justify">
                    <span>All News</span><span>/ 185</span>
                </a> </li>
            <li> <a href="blog-right.html" title="" class="content-justify">
                    <span>Tongue bresaola</span><span>/ 25</span>
                </a> </li>
            <li> <a href="blog-right.html" title="" class="content-justify">
                    <span>leberkas picanha</span><span>/ 18</span>
                </a> </li>
            <li> <a href="blog-right.html" title="" class="content-justify">
                    <span>Ham hock land</span><span>/ 13</span>
                </a> </li>
            <li> <a href="blog-right.html" title="" class="content-justify">
                    <span>Ground round</span><span>/ 113</span>
                </a> </li>
        </ul>
    </div>

    <div class="widget widget_products widget_popular_entries">
        <h3 class="widget-title">Sale products</h3>
        <ul class="media-list">
            <li class="media"> <a class="media-left media-middle" href="shop-product-right.html">
                    <img class="media-object muted_background" src="images/shop/01.png" alt="">
                </a>
                <div class="media-body media-middle">
                    <h4 class="entry-title"> <a href="shop-product-right.html">Narcissist Treatment</a> </h4>
                    <div class="star-rating  small-stars" title="Rated 4.00 out of 5"> <span style="width:80%">
                            <strong class="rating">4.00</strong> out of 5
                        </span> </div> <span class="price small-text">
                        <del>
                            <span class="amount">$59</span> </del> <ins>
                            <span class="amount">$49</span>
                        </ins> </span>
                </div>
            </li>
            <li class="media"> <a class="media-left media-middle" href="shop-product-right.html">
                    <img class="media-object muted_background" src="images/shop/02.png" alt="">
                </a>
                <div class="media-body media-middle">
                    <h4 class="entry-title"> <a href="shop-product-right.html">Dealing with Criticis</a> </h4>
                    <div class="star-rating small-stars" title="Rated 3.00 out of 5"> <span style="width:70%">
                            <strong class="rating">3.00</strong> out of 5
                        </span> </div> <span class="price small-text">

                        <del>
                            <span class="amount">$125</span> </del> <ins>
                            <span class="amount">$99</span>
                        </ins> </span>
                </div>
            </li>
            <li class="media"> <a class="media-left media-middle" href="shop-product-right.html">
                    <img class="media-object muted_background" src="images/shop/03.png" alt="">
                </a>
                <div class="media-body media-middle">
                    <h4 class="entry-title"> <a href="shop-product-right.html">Insights into Life</a> </h4>
                    <div class="star-rating  small-stars" title="Rated 2.00 out of 5"> <span style="width:40%">
                            <strong class="rating">2.00</strong> out of 5
                        </span> </div> <span class="price small-text">
                        <del>
                            <span class="amount">$46</span> </del> <ins>
                            <span class="amount">$33</span>
                        </ins> </span>
                </div>
            </li>
        </ul>
    </div>
</aside>