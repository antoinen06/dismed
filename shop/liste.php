<section id="products" class="ls section_padding_top_150 section_padding_bottom_130">
				<div class="container">
				<div class="row">
		<?php
	if(isset($_GET['add']))
	{
		if(($_GET['add']==0)){
			?>
			<div class="alert alert-danger"> le produit est deja dans votre panier</div>
			<?php
		}else
		{
			?>
			<div class="alert alert-success"> le produit est ajouter avec success</div>
			<?php
		}
	}
			?>
		</div>
					<div class="row">
						<div class="col-sm-12">
							<div class="shop-sorting">
									<form class="form-inline content-justify vertical-center content-margins">
									<div> Showing 1-6 of 36 results </div>
									<div class="form-group select-group"> <select aria-required="true" id="date" name="date" class="choice empty form-control">
		                        <option value="" disabled selected data-default>Default Sorting</option>
		                        <option value="value">by Value</option>
		                        <option value="date">by Date</option>
		                        <option value="popular">by Popularity</option>
		                    </select> <i class="fa fa-angle-down theme_button color1 no_bg_button" aria-hidden="true"></i> </div>
								</form>
							</div>
							<div class="columns-3">
								<ul id="products" class="products list-unstyled">
                                <!-- foreach -->
								<?php
								$produit =listeProduit();
                                    while($row = mysqli_fetch_row($produit))
                                    {
                                ?>
									<li class="product type-product">
										<div class="vertical-item content-padding text-center with_border">
											<div class="item-media muted_background bottommargin_30">  <!-- image produit--> <img src="images/produits/<?php echo $row[6] ?>" alt="<?php echo $row[1] ?>" /> </div>
											<div class="item-content">
												<h4 class="hover-color2 bottommargin_0"> <a href="shop-product-right.html"><?php echo $row[1] ?> <!-- nom produit--></a> </h4>
												<p class="price semibold"> 
                                                    <!-- <del>
											<span class="amount">$12.00</span>
                                        </del> -->
                                         <ins>
											<span class="amount" ><?php echo $row[4] ?> </span>  <!-- prix produit-->
										</ins> </p>
												<p><?php echo $row[3] ?></p>  <!-- DESC produit-->
												<p class="topmargin_30"> <a href="Controller/ProduitController.php?id=<?php echo $row[0] ?>&type=addListe" class="theme_button color2 inverse min_width_button">Add to Cart</a> </p>  <!-- commande add-->
											</div>
										</div>
                                    </li>
                                    <!-- endforeach -->
                                    <?php
                                    
                                    }
                                ?>
								</ul>
							</div>
							<!-- eof .columns-* -->
							<div class="row">
								<div class="col-sm-12 text-center">
									<ul class="pagination with_border">
										<li class="disabled"><a href="#"><span class="sr-only">Prev</span><i class="fa fa-angle-left" aria-hidden="true"></i></a></li>
										<li class="active"><a href="#">1</a></li>
										<li><a href="#">2</a></li>
										<li><a href="#">3</a></li>
										<li><a href="#">4</a></li>
										<li><a href="#"><span class="sr-only">Next</span><i class="fa fa-angle-right" aria-hidden="true"></i></a></li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>