<?php
$element = substr($_SERVER['REQUEST_URI'],strpos($_SERVER['REQUEST_URI'],'/')+1);
// echo $element;
$insertURL = false;
if(substr($element,1,9)=='index.php' ||substr($element,1,1)=='#' ||substr($element,0,1)==''){
    $insertURL = true;
}
if($insertURL)
{
?>
<header id="home " class="page_header header_color toggler_left">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 display_table">
                <div class="header_right_buttons display_table_cell">
                    <nav class="mainmenu_wrapper">
                        <ul class="mainmenu nav sf-menu">
                            <li> <a href="/"> <img src="images/2.png" alt=""></a></li>
                        </ul>
                    </nav>
                </div>
                <div class="header_right_buttons display_table_cell">
                    <!-- main nav start -->
                    <nav class="mainmenu_wrapper">
                        <ul class="mainmenu nav sf-menu">
                            <li> <a href="#about">Accueil</a> </li>
                            <li> <a href="#home">Présentation</a> </li>
                            <li> <a href="#pipeline">Promotion médicale</a> </li>
                            <li> <a href="#features">Pourquoi nous choisir ?</a> </li>
                            <li> <a href="#products">Produits</a> </li>
                            <li> <a href="#contact">Contact</a> </li>
                            <li> <a href="#about"></a>
                            <style>
                            #ex4 .p1[data-count]:after {
                            position: absolute;
                            right: 10%;
                            top: 8%;
                            content: attr(data-count);
                            font-size: 40%;
                            padding: .2em;
                            border-radius: 50%;
                            line-height: 1em;
                            color: white;
                            background: rgba(255, 0, 0, 0.85);
                            text-align: center;
                            min-width: 1em;
                            }
                            </style>
                        </li>
                        <li> <a href="/panier" class="header-button">
                        <div id="ex4">
                            <span class="p1 fa-stack fa-2x has-badge" id="count_cart" data-count="<?php echo count($_SESSION['panier']);  ?>">
                                <i class="fa fa-shopping-basket" aria-hidden="true"></i>
                        </span>
                        </div>
                            </a> 
                       
                        </li>

                        </ul>
                    </nav>
                    <!-- eof main nav -->
                    <!-- header toggler<span class="toggle_menu"></span> -->
                </div>
                <div class="header_right_buttons cs display_table_cell text-right">
                    <ul class="inline-list menu greylinks">
                   
                </div>
            </div>
        </div>
    </div>
</header>
<?php
}else{
?>
<header id="home " class="page_header header_color toggler_left">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 display_table">
                <div class="header_right_buttons display_table_cell">
                    <nav class="mainmenu_wrapper">
                        <ul class="mainmenu nav sf-menu">
                            <li> <a href="/"> <img src="images/2.png" alt=""></a></li>
                        </ul>
                    </nav>
                </div>
                <div class="header_right_buttons display_table_cell">
                    <!-- main nav start -->
                    <nav class="mainmenu_wrapper">
                        <ul class="mainmenu nav sf-menu">
                            <li> <a href="/">Accueil</a> </li>
                            <li> <a href="/#home">Présentation</a> </li>
                            <li> <a href="/#pipeline">Promotion médicale</a> </li>
                            <li> <a href="/#features">Pourquoi nous choisir ?</a> </li>
                            <li class="active"> <a href="/produit">Produits</a> </li>
                            <li> <a href="/#contact">Contact</a> </li>
                            <li> <a href="/#about"></a>
                            <style>
                            #ex4 .p1[data-count]:after {
                            position: absolute;
                            right: 10%;
                            top: 8%;
                            content: attr(data-count);
                            font-size: 40%;
                            padding: .2em;
                            border-radius: 50%;
                            line-height: 1em;
                            color: white;
                            background: rgba(255, 0, 0, 0.85);
                            text-align: center;
                            min-width: 1em;
                            }
                            </style>
                            </li>
                        </ul>
                    </nav>
                    <!-- eof main nav -->
                    <!-- header toggler<span class="toggle_menu"></span> -->
                </div>
                <div class="header_right_buttons cs display_table_cell text-right">
                    <ul class="inline-list menu greylinks">
                    <li> <a href="/panier" class="header-button">
                        <div id="ex4">
                            <span class="p1 fa-stack fa-2x has-badge" id="count_cart" data-count="<?php echo count($_SESSION['panier']);  ?>">
                                <i class="fa fa-shopping-basket" aria-hidden="true"></i>
                        </span>
                        </div>
                            </a> 
                       
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</header>
<?php
}
?>