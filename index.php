<?php
session_start();
?>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js">
<!--<![endif]-->


<!-- Mirrored from webdesign-finder.com/html/pharma/index-single.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 30 Jan 2019 14:41:02 GMT -->
<head>
	<link rel="shortcut icon" type="image/png" href="images/SmallLogo.png"/>
	<title>Dismed Pharma</title>
	<meta charset="utf-8">
	<!--[if IE]>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<![endif]-->
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/animations.css">
	<link rel="stylesheet" href="css/fonts.css">
	<link rel="stylesheet" href="css/main.css" class="color-switcher-link">
	<link rel="stylesheet" href="css/shop.css">
	<script src="js/vendor/modernizr-2.6.2.min.js"></script>
	<!--[if lt IE 9]>
		<script src="js/vendor/html5shiv.min.js"></script>
		<script src="js/vendor/respond.min.js"></script>
		<script src="js/vendor/jquery-1.12.4.min.js"></script>
	<![endif]-->
</head>

<body>
	<!--[if lt IE 9]>
		<div class="bg-danger text-center">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/" class="highlight">upgrade your browser</a> to improve your experience.</div>
	<![endif]-->
	<div class="preloader">
		<div class="preloader_image"></div>
	</div>
	<!-- search modal -->
	<div class="modal" tabindex="-1" role="dialog" aria-labelledby="search_modal" id="search_modal"> <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		<span aria-hidden="true">
			<i class="rt-icon2-cross2"></i>
		</span>
	</button>
		<div class="widget widget_search">
			<form method="get" class="searchform search-form form-inline" action="http://webdesign-finder.com/html/pharma/">
				<div class="form-group bottommargin_0"> <input type="text" value="" name="search" class="form-control" placeholder="Search keyword" id="modal-search-input"> </div> <button type="submit" class="theme_button">Search</button> </form>
		</div>
	</div>
	<!-- Unyson messages modal -->
	<div class="modal fade" tabindex="-1" role="dialog" id="messages_modal">
		<div class="fw-messages-wrap ls with_padding">
			<!-- Uncomment this UL with LI to show messages in modal popup to your user: -->
			<!--
		<ul class="list-unstyled">
			<li>Message To User</li>
		</ul>
		-->
		</div>
	</div>
	<!-- eof .modal -->
	<!-- wrappers for visual page editor and boxed version of template -->
	<div id="canvas">
		<div id="box_wrapper">
			<!-- template sections -->
			<!--  -->
			<?php
				require_once 'navbar.php';
			?>
			<section id="about" class="intro_section page_mainslider ds color">
				<div class="flexslider" data-dots="true" data-nav="false">
					<ul class="slides">
						<li> <img src="images/slide01.jpg" alt="">
							<div class="container">
								<div class="row">
									<div class="col-sm-12 text-center">
										<div class="slide_description_wrapper">
											<div class="slide_description">
												<div class="intro-layer with_corner_border with_padding huge-padding" data-animation="slideExpandUp">
													<h2 class="highlight">Dismed Pharma</h2>
													<p> Société sénégalaise spécialisée dans la promotion de médicaments </br> ayant une expérience de plus de 15 ans. </p> <a href="#home" class="theme_button color2 inverse margin_0">
											Présentation
										</a> <span class="bottom_corners"></span> </div>
											</div>
											<!-- eof .slide_description -->
										</div>
										<!-- eof .slide_description_wrapper -->
									</div>
									<!-- eof .col-* -->
								</div>
								<!-- eof .row -->
							</div>
							<!-- eof .container -->
						</li>
						<li> <img src="images/slide02.jpg" alt="">
							<div class="container">
								<div class="row">
									<div class="col-sm-12 text-center">
										<div class="slide_description_wrapper">
											<div class="slide_description">
												<div class="intro-layer with_corner_border with_padding huge-padding" data-animation="slideExpandUp">
													<h5 class="thin"> Nous faisons aussi de la </h5>
													<h2 class="highlight">Promotion Médicale</h2>
													<p> Nous disposons d’un vaste réseau de délégués résidants </br> dans toutes les régions du Sénégal. </p> <a href="#pipeline" class="theme_button color2 inverse margin_0">
											Voir Plus
										</a> <span class="bottom_corners"></span> </div>
											</div>
											<!-- eof .slide_description -->
										</div>
										<!-- eof .slide_description_wrapper -->
									</div>
									<!-- eof .col-* -->
								</div>
								<!-- eof .row -->
							</div>
							<!-- eof .container -->
						</li>
						<li> <img src="images/slide03.jpg" alt="">
							<div class="container">
								<div class="row">
									<div class="col-sm-12 text-center">
										<div class="slide_description_wrapper">
											<div class="slide_description">
												<div class="intro-layer with_corner_border with_padding huge-padding" data-animation="slideExpandUp">
													
													<h2 class="highlight"> Distribution Médicale </h2>
													<p>DISMED PHARMA c’est aussi la distribution de matériels médicaux spécialisés </br> pour les dentistes et gynécologues et de consommables médicaux </p> <a href="#products" class="theme_button color2 inverse margin_0">
											Voir Plus
										</a> <span class="bottom_corners"></span> </div>
											</div>
											<!-- eof .slide_description -->
										</div>
										<!-- eof .slide_description_wrapper -->
									</div>
									<!-- eof .col-* -->
								</div>
								<!-- eof .row -->
							</div>
							<!-- eof .container -->
						</li>
					</ul>
				</div>
				<!-- eof flexslider -->
			</section>
			<section id="home" class="ls section_padding_top_150 section_padding_bottom_150">
				<div class="container">
					<div class="row">
						<div class="col-sm-12 text-center">
							<h2 class="section_header"> Bienvenue sur notre site ! </h2></br></br>
							<p style="text-align:center">Nous sommes une société sénégalaise évoluant dans le secteur de la santé depuis plus de 15 années. Nous vous proposons différents services comme la promotion médicale permettant ainsi aux structures de santé d'avoir un accompagnement en ressources humaines des plus fiables. Mais aussi de la distribution de matériels et de consommables médicaux pour permettre aux équipes médicaux de s'équiper au mieux.</p></br></br>
						</div>
					</div>
					<div class="row columns_margin_bottom_20">
						<div class="col-md-4 col-sm-6">
							<div class="teaser hover_icon with_padding big-padding with_border rounded text-center">
								<div class="teaser_icon size_big highlight"> <i class="rt-icon2-user"></i> </div>
								<h4 class="topmargin_20 hover-color2">Pharma Team</h4>
								<p class="content-3lines-ellipsis">Une équipe sérieuse et dynamique toujours à l'écoute de ses clients.</p>
								
							</div>
						</div>
						<div class="col-md-4 col-sm-6">
							<div class="teaser hover_icon with_padding big-padding with_border rounded text-center with_corner_border">
								<div class="teaser_icon size_big highlight"> <i class="rt-icon2-bulb"></i> </div>
								<h4 class="topmargin_20 hover-color2">Pharma Focus</h4>
								<p class="content-3lines-ellipsis">Nous cherchons toujours à satisfaire le client.</p>
								 <span class="bottom_corners"></span> </div>
						</div>
						<div class="col-md-4 col-sm-6 col-sm-offset-3 col-md-offset-0">
							<div class="teaser hover_icon with_padding big-padding with_border rounded text-center">
								<div class="teaser_icon size_big highlight"> <i class="rt-icon2-paperplane"></i> </div>
								<h4 class="topmargin_20 hover-color2">Pharma Livraison</h4>
								<p class="content-3lines-ellipsis">Livraison rapide des produits.</p>
								
							</div>
						</div>
					</div>
				</div>
			</section>
			<section id="pipeline" class="ds color background_cover page_features section_padding_top_150 section_padding_bottom_150">
				<div class="container">
					<div class="row">
						<div class="col-lg-6 col-md-8 col-md-offset-4 col-lg-offset-6">
							<h2 class="section_header">Promotion Médicale</h2>
							<p class="highlight bottommargin_20"><strong>DIMED PHARMA fournit à ses partenaires laboratoires pharmaceutiques :</strong></br></br></p>
							<p style="color: #a8a8a8"> <strong>- Des délégués médicaux de qualité dont leur formation continue est assurée par un pharmacien responsable et des experts en marketing et communication.
								</br></br>-	Une assistance en affaires réglementaires au niveau de la DPM. 
								</br></br>-	Les statistiques de ventes mensuelles des produits, ainsi que leur interprétation.
								</br></br>- Les couvertures de stocks chez les grossistes afin de minimiser les ruptures. 
								</br></br>- Information et réalisation d’étude de marchés  local sur les médicaments (DCI) 
								</br></br>- Assistance sur les appels d’offres et marchés publics. </strong></p>
							<!-- <div class="row topmargin_40 columns_margin_bottom_20">
								<div class="col-sm-4 col-xs-6 col-xxs-12">
									<div class="vertical-item teaser clear-media hover_bg_teaser ls with_padding rounded with_shadow min_height_165">
										<p class="big highlight2 bottommargin_20 topmargin_5">01</p>
										<h5 class="margin_0">Research &amp; Preclinical</h5>
										<div class="media-links clear-media"> <a href="pipeline-single.html" class="abs-link"></a> </div>
									</div>
								</div>
								<div class="col-sm-4 col-xs-6 col-xxs-12">
									<div class="vertical-item teaser clear-media hover_bg_teaser ls with_padding rounded with_shadow min_height_165">
										<p class="big highlight2 bottommargin_20 topmargin_5">02</p>
										<h5 class="margin_0">Starting Phase</h5>
										<div class="media-links clear-media"> <a href="pipeline-single.html" class="abs-link"></a> </div>
									</div>
								</div>
								<div class="col-sm-4 col-xs-6 col-xxs-12">
									<div class="vertical-item teaser clear-media hover_bg_teaser ls with_padding rounded with_shadow min_height_165">
										<p class="big highlight2 bottommargin_20 topmargin_5">03</p>
										<h5 class="margin_0">Medium Phase</h5>
										<div class="media-links clear-media"> <a href="pipeline-single.html" class="abs-link"></a> </div>
									</div>
								</div>
								<div class="col-sm-4 col-xs-6 col-xxs-12">
									<div class="vertical-item teaser clear-media hover_bg_teaser ls with_padding rounded with_shadow min_height_165">
										<p class="big highlight2 bottommargin_20 topmargin_5">04</p>
										<h5 class="margin_0">Finishing Phase</h5>
										<div class="media-links clear-media"> <a href="pipeline-single.html" class="abs-link"></a> </div>
									</div>
								</div>
								<div class="col-sm-4 col-xs-6 col-xxs-12">
									<div class="vertical-item teaser clear-media hover_bg_teaser ls with_padding rounded with_shadow min_height_165">
										<p class="big highlight2 bottommargin_20 topmargin_5">05</p>
										<h5 class="margin_0">After Drug Approval</h5>
										<div class="media-links clear-media"> <a href="pipeline-single.html" class="abs-link"></a> </div>
									</div>
								</div>
								<div class="col-sm-4 col-xs-6 col-xxs-12">
									<div class="vertical-item teaser clear-media hover_bg_teaser ls with_padding rounded with_shadow min_height_165">
										<p class="big highlight2 bottommargin_20 topmargin_5">06</p>
										<h5 class="margin_0">Drugs Production</h5>
										<div class="media-links clear-media"> <a href="pipeline-single.html" class="abs-link"></a> </div>
									</div>
								</div>
							</div> -->
						</div>
					</div>
				</div>
			</section>
			<section id="features" class="ls section_padding_top_150 section_padding_bottom_130 columns_margin_bottom_30">
				<div class="container">
					<div class="row">
						<div class="col-md-4">
							<h2 class="section_header">Pourquoi nous choisir ?</h2></div>
						<div class="col-md-4 col-sm-6">
							<div class="teaser media hover_icon">
								<div class="media-left">
									<div class="teaser_icon rounded main_bg_color size_small"> <i class="rt-icon2-diamond2"></i> </div>
								</div>
								<div class="media-body toppadding_10">
									<h5 class="hover-color2">Nouvelles technologies</h5>
									
								</div>
							</div>
							<div class="teaser media hover_icon">
								<div class="media-left">
									<div class="teaser_icon rounded main_bg_color size_small"> <i class="rt-icon2-cloud"></i> </div>
								</div>
								<div class="media-body toppadding_10">
									<h5 class="hover-color2">Prendre soin de la nature</h5>
									
								</div>
							</div>
						</div>
						<div class="col-md-4 col-sm-6">
							<div class="teaser media hover_icon">
								<div class="media-left">
									<div class="teaser_icon rounded main_bg_color size_small"> <i class="rt-icon2-tag2"></i> </div>
								</div>
								<div class="media-body toppadding_10">
									<h5 class="hover-color2">Prix ​​équitables</h5>
									
								</div>
							</div>
							<div class="teaser media hover_icon">
								<div class="media-left">
									<div class="teaser_icon rounded main_bg_color size_small"> <i class="rt-icon2-cloud"></i> </div>
								</div>
								<div class="media-body toppadding_10">
									<h5 class="hover-color2">Satisfaction de la clientièle élevée</h5>
									
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
			<section id="products" class="ds color parallax page_gallery section_padding_top_100 section_padding_bottom_150 columns_padding_25">
				<div class="container">
					<div class="row display_table_md">
						<div class="col-md-6 col-md-push-6 display_table_cell_md"> <img src="images/gallery/01.jpg" alt="" class="rounded"> </div>
						<div class="col-md-6 col-md-pull-6 display_table_cell_md">
							<h2 class="section_header">Nos produits</h2>
							<p class="highlight bottommargin_20"><strong>Dismed Pharma est aussi dans la distribution de matériels médicaux spécialisés pour les dentistes, gynécologues, et de consommables médicaux </strong></p>
							<p>Nous vous proposons une large gamme dédiée à l’équipement médical, pour permettre au corps médical de s’équiper au mieux. Nous proposons aussi des dispositifs médicaux destinés aux professionnels de santé ainsi qu’aux particuliers.
								Nous vous accompagnons dans le choix de vos fournitures médicales, accessoires médicaux.</p>
							<!-- <p class="topmargin_30"> <a href="#contact" class="theme_button color2 inverse" target="_bank">Nos produits</a> </p> -->
							<p class="topmargin_30"> <a href="/produit" class="theme_button color2 inverse">Nos produits</a> </p>
						</div>
					</div>
					<div class="row topmargin_30">
						<div class="col-sm-12">
							<div class="owl-carousel gallery-carousel" data-responsive-lg="5" data-responsive-md="4" data-responsive-sm="3" data-responsive-xs="1" data-nav="true">
								<div class="gallery-item rounded">
									<div class="item-media rounded overflow_hidden"> <img src="images/gallery/02.jpg" alt="">
										<div class="media-links inverse"> <a href="images/gallery/02.jpg" class="abs-link prettyPhoto" title="" data-gal="prettyPhoto[gal]"></a> </div>
									</div>
								</div>
								<div class="gallery-item rounded">
									<div class="item-media rounded overflow_hidden"> <img src="images/gallery/03.jpg" alt="">
										<div class="media-links inverse"> <a href="images/gallery/03.jpg" class="abs-link prettyPhoto" title="" data-gal="prettyPhoto[gal]"></a> </div>
									</div>
								</div>
								<div class="gallery-item rounded">
									<div class="item-media rounded overflow_hidden"> <img src="images/gallery/04.jpg" alt="">
										<div class="media-links inverse"> <a href="images/gallery/04.jpg" class="abs-link prettyPhoto" title="" data-gal="prettyPhoto[gal]"></a> </div>
									</div>
								</div>
								<div class="gallery-item rounded">
									<div class="item-media rounded overflow_hidden"> <img src="images/gallery/05.jpg" alt="">
										<div class="media-links inverse"> <a href="images/gallery/05.jpg" class="abs-link prettyPhoto" title="" data-gal="prettyPhoto[gal]"></a> </div>
									</div>
								</div>
								<div class="gallery-item rounded">
									<div class="item-media rounded overflow_hidden"> <img src="images/gallery/06.jpg" alt="">
										<div class="media-links inverse"> <a href="images/gallery/06.jpg" class="abs-link prettyPhoto" title="" data-gal="prettyPhoto[gal]"></a> </div>
									</div>
								</div>
								<div class="gallery-item rounded">
									<div class="item-media rounded overflow_hidden"> <img src="images/gallery/07.jpg" alt="">
										<div class="media-links inverse"> <a href="images/gallery/07.jpg" class="abs-link prettyPhoto" title="" data-gal="prettyPhoto[gal]"></a> </div>
									</div>
								</div>
								<div class="gallery-item rounded">
									<div class="item-media rounded overflow_hidden"> <img src="images/gallery/08.jpg" alt="">
										<div class="media-links inverse"> <a href="images/gallery/08.jpg" class="abs-link prettyPhoto" title="" data-gal="prettyPhoto[gal]"></a> </div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
			<section id="contact" class="ls columns_padding_25 section_padding_top_100 section_padding_bottom_100">
					<div class="container">
						<div class="row">
							<div class="col-sm-12 text-center">
								<h3 class="module-header">Contactez-nous !</h3>
							</div>
						</div>
						<div class="row">
							<div class="col-md-8 to_animate" data-animation="scaleAppear">
								<form class="contact-form columns_padding_10 bottommargin_40" method="post" action="http://webdesign-finder.com/html/pharma/">
									<div class="row">
										<div class="col-sm-6">
											<div class="form-group bottommargin_0"> <label for="name">Full Name <span class="required">*</span></label> <i class="fa fa-user highlight2" aria-hidden="true"></i> <input type="text" aria-required="true" size="30" value="" name="name" id="name" class="form-control" placeholder="Nom Complet">											</div>
										</div>
										<div class="col-sm-6">
											<div class="form-group bottommargin_0"> <label for="phone">Phone Number<span class="required">*</span></label> <i class="fa fa-phone highlight2" aria-hidden="true"></i> <input type="text" aria-required="true" size="30" value="" name="phone" id="phone" class="form-control" placeholder="Numéro  de Téléphone">											</div>
										</div>
										<div class="col-sm-6">
											<div class="form-group bottommargin_0"> <label for="email">Email address<span class="required">*</span></label> <i class="fa fa-envelope highlight2" aria-hidden="true"></i> <input type="email" aria-required="true" size="30" value="" name="email" id="email" class="form-control" placeholder="Adresse mail">											</div>
										</div>
										<div class="col-sm-6">
											<div class="form-group bottommargin_0"> <label for="subject">Subject<span class="required">*</span></label> <i class="fa fa-flag highlight2" aria-hidden="true"></i> <input type="text" aria-required="true" size="30" value="" name="subject" id="subject" class="form-control" placeholder="Votre Sujet">											</div>
										</div>
										<div class="col-sm-12">
											<div class="contact-form-message form-group bottommargin_0"> <label for="message">Message</label> <i class="fa fa-comment highlight2" aria-hidden="true"></i> <textarea aria-required="true" rows="3" cols="45" name="message" id="message" class="form-control" placeholder="Message"></textarea> </div>
										</div>
										<div class="col-sm-12 bottommargin_0">
											<div class="contact-form-submit topmargin_10"> <button type="submit" id="contact_form_submit" name="contact_submit" class="theme_button color2 wide_button margin_0">Send message</button> </div>
										</div>
									</div>
								</form>
							</div>
							<div class="col-md-4 to_animate" data-animation="scaleAppear">
								<ul class="list1 no-bullets no-top-border no-bottom-border">
									<li>
										<div class="media">
											<div class="media-left"> <i class="rt-icon2-phone5 highlight2 fontsize_18"></i> </div>
											<div class="media-body">
												<h5 class="media-heading grey">Telephone:</h5> +221 76 226 11 56 </div>
										</div>
									</li>
									<li>
										<div class="media">
											<div class="media-left"> <i class="rt-icon2-mail highlight2 fontsize_18"></i> </div>
											<div class="media-body greylinks">
												<h5 class="media-heading grey">Email:</h5> <a href="mailto:contact@dismedpharma.com">contact@dismedpharma.com</a> </div>
										</div>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</section>
			
		<?php
			require_once 'footer.php';
		?>
		</div>
		<!-- eof #box_wrapper -->
	</div>
	<!-- eof #canvas -->
	<script src="js/compressed.js"></script>
	<script src="js/main.js"></script>
	<script src="js/switcher.js"></script>
	<script>
		$(document).ready(function(){
			document.getElementById("home").focus();
		});
	</script>
</body>


</html>